import {apiPost, apiGet} from './helper';

import {SEND_CODE_URL, VERIFY_CODE_URL, GET_GIFT_URL, SEND_GIFT_URL} from './urls';

const defaultHeader =  {
    'Content-Type': 'application/json'
}

export const handleSendCode = (phoneNumber, callBackFn) => {
    let payload = {
        'phone_number' : phoneNumber
    };

    apiPost(SEND_CODE_URL, defaultHeader, payload, callBackFn);
}

export const handleVerifyCode = (phoneNumber, code, callBackFn) => {
    let payload = {
        'phone_number' : phoneNumber,
        'code' : code,
    };

    apiPost(VERIFY_CODE_URL, defaultHeader, payload, callBackFn);
}

export const handleGetGift = (token, callBackFn) => {
    let headerWithToken = {
        ...defaultHeader,
        'jwt': token
    };

    apiGet(GET_GIFT_URL, headerWithToken, callBackFn);
} 

export const handleSendGift = (token, email, callBackFn) => {
    let headerWithToken = {
        ...defaultHeader,
        'jwt': token
    };

    let payload = {
        'email' : email
    };

    apiPost(SEND_GIFT_URL, headerWithToken, payload, callBackFn);
}
