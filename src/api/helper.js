const handleResponse = (response, callBackFn) => (response.text().then(text => {
    const data = text && JSON.parse(text);

    console.log(data);

    return callBackFn(data, response.status)
}));

export const apiGet = (url, headers, callBackFn) => fetch(`${url}`, {
    method: 'GET',
    headers: new Headers(headers),
})
.then(v => handleResponse(v, callBackFn))
.catch(err => console.log(err));


export const apiPost = (url, headers, obj, callBackFn) => fetch(`${url}`, {
    method: 'POST',
    body: JSON.stringify(obj),
    headers: new Headers(headers),
})
.then(v => handleResponse(v, callBackFn))
.catch(err => console.log(err));
