const BASE_URL = `http://navidad.hellsoft.co:3042`

export const SEND_CODE_URL = `${BASE_URL}/verification/send`
export const VERIFY_CODE_URL = `${BASE_URL}/verification/verify`

export const GET_GIFT_URL = `${BASE_URL}/gift`
export const SEND_GIFT_URL = `${BASE_URL}/gift/send`