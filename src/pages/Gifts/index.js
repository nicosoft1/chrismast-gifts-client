import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { ThemeProvider } from '@mui/material/styles';
import { useState } from 'react';
import { RedeemOutlined } from '@material-ui/icons';
import { FormControlLabel } from '@material-ui/core';
import { Slide, Switch } from '@mui/material';
import { handleGetGift, handleSendGift } from '../../api';
import { PacmanLoader } from 'react-spinners';

const generateGiftContainer = (giftText, token) => {
    const sendGiftCallBack = (payload, status) => {
        if (status === 200) {
            // Add notification of success
            return;
        }

        if (status === 400) {
            alert("Bad formatted email");
            return;
        }

        if (status === 401) {
            alert("Stop pushing this button");
            return;
        }

        console.log(payload);
        alert("Sorry :( ");
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = new FormData(event.currentTarget);

        handleSendGift(token, data.get("email"), sendGiftCallBack)

        console.log(data.get("email"))
    };

    return (
        <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="flex-start"
        >
            <Grid item>
                <Typography component="h1" variant="h5">
                    Feliz Navidad 🎄
                </Typography>
            </Grid>
            <Grid item>
                <br />
            </Grid>
            <Grid item>
                <Typography component="h1" variant="h5">
                    {giftText}
                </Typography>
            </Grid>
            <Grid item>
                <br />
            </Grid>
        </Grid>

    )
}

export default function Gifts({ theme, token }) {
    const [checked, setChecked] = useState(false);
    const [giftText, setGiftText] = useState("");
    const [giftRequested, setGiftRequested] = useState(false);

    const handleChange = () => {
        setChecked((prev) => !prev);
    };

    const getGiftCallBack = (payload, status) => {
        if (status === 200) {
            setGiftRequested(true);
            setGiftText(payload.gift);
            return;
        }

        console.log(payload);
    }

    if (!giftRequested) {
        handleGetGift(token, getGiftCallBack);
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    {
                        giftRequested ?
                            (
                                <>
                                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }} >
                                        <RedeemOutlined fontSize='large' />
                                    </Avatar>

                                    <Box sx={{ height: 180 }}>
                                        <FormControlLabel
                                            control={<Switch checked={checked} onChange={handleChange} />}
                                            label="Recibir Regalo"
                                        />
                                        <Box sx={{ display: 'flex' }}>
                                            <Slide direction="up" in={checked} mountOnEnter unmountOnExit>
                                                {generateGiftContainer(giftText, token)}
                                            </Slide>
                                        </Box>
                                    </Box>
                                </>
                            ) : (
                                <PacmanLoader loading={!giftRequested} colo={"#ffffff"} />
                            )
                    }

                </Box>
            </Container>
        </ThemeProvider>
    );
}