import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { ThemeProvider } from '@mui/material/styles';
import { useState } from 'react';
import { AcUnitOutlined } from '@material-ui/icons';
import {handleSendCode, handleVerifyCode} from '../../api/index';

function VerifyCode({ setIsCodeSent, setToken, phoneNumber}) {
    const sendCodeCallBack = (payload, status) => { 
        if (status === 403) {
            alert("Too much tries");
            return;
        }

        console.log(payload);
    }

    const verifyCodeCallback = (payload, status ) => {
        if (status === 200) {
            setToken(payload.jwt);
            return;
        }

        if (status === 401) {
            alert("Sorry the code is not the right one, try again");
            return;
        }

        console.log(payload);
        alert("Sorry :( ");
    }

    const handleSendCodeAgain = () => {
        handleSendCode(phoneNumber, sendCodeCallBack);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        
        const data = new FormData(event.currentTarget);

        handleVerifyCode(phoneNumber, data.get("verification-code"), verifyCodeCallback);

        console.log(data.get("verification-code"))
    };

    return (
        <>
            <Typography component="h1" variant="h5">
                Escribe el código que enviamos 🤠
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="verification-code"
                    label="Code"
                    name="verification-code"
                    type="number"
                    autoFocus
                />

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Verificar
                </Button>

                <Grid
                    container
                    direction="column"
                    justifyContent="flex-end"
                    alignItems="flex-end"
                >
                    <Grid item>
                        <Button
                            variant='text'
                            onClick={handleSendCodeAgain}
                        >
                            Enviar código de nuevo
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            variant='text'
                            onClick={
                                _ => setIsCodeSent(false)
                            }
                        >
                            Usar otro número
                        </Button>
                    </Grid>
                </Grid>

            </Box>
        </>
    );
}

function SendCode({ setIsCodeSent, setPhoneNumber }) {
    const sendCodeCallBack = (payload, status) => {
        if (status === 200) {
            setIsCodeSent(true);
            return;
        }

        if (status === 400) {
            alert("Bad formatted number");
            return;
        }  

        if (status === 401) {
            alert("Not existing number");
            return;
        } 

        console.log(payload);
        alert("Sorry :( ");
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = new FormData(event.currentTarget);

        setPhoneNumber(data.get("phone-number"));
        handleSendCode(data.get("phone-number"), sendCodeCallBack);

        console.log(data.get("phone-number"));
    };

    return (
        <>
            <Typography component="h1" variant="h5">
                Usa tu número de teléfono para validar tu identidad
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="phone-number"
                    label="Phone Number"
                    name="phone-number"
                    autoComplete="phone-number"
                    type="number"
                    autoFocus
                />

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Enviar
                </Button>

            </Box>
        </>
    );
}

export default function Login({theme, setToken}) {
    const [isCodeSent, setIsCodeSent] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState("");

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }} >
                        <AcUnitOutlined fontSize='large' />
                    </Avatar>
                    {
                        isCodeSent ?
                            <VerifyCode
                                setIsCodeSent={setIsCodeSent}
                                setToken={setToken}
                                phoneNumber={phoneNumber}
                            /> :
                            <SendCode
                                setIsCodeSent={setIsCodeSent}
                                setPhoneNumber={setPhoneNumber}
                            />
                    }
                </Box>
            </Container>
        </ThemeProvider>
    )
}