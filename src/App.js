import React, { useState } from 'react';
import Login from './pages/Login';
import Gifts from './pages/Gifts';

import { createTheme } from '@mui/material/styles';


const theme = createTheme({
  palette: {
    background: {
      default: "#222222"
    },
    text: {
      primary: "#ffffff"
    }
  }
});

function App() {
  const [token, setToken] = useState();

  if(!token) {
    return <Login theme={theme} setToken={setToken} />
  }

  return (
    <Gifts token={token} theme={theme}/>
  );
}

export default App;